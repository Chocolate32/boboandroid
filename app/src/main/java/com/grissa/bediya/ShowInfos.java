package com.grissa.bediya;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ShowInfos extends AppCompatActivity {
    public static final String EXTRA_FULL_NAME="full name";
    public static final String EXTRA_EMAIL="email";
    public static final String EXTRA_PHONE_NUMBER="0000123456789";
    public static final String EXTRA_INSTITUTION="School";
    public static final String EXTRA_IS_MALE="other";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_infos);
        Intent intent=getIntent();
        boolean isMale=intent.getBooleanExtra(EXTRA_IS_MALE,true);
        String fullName=intent.getStringExtra(EXTRA_FULL_NAME);
        String email=intent.getStringExtra(EXTRA_EMAIL);
        String phone=intent.getStringExtra(EXTRA_PHONE_NUMBER);
        String institution=intent.getStringExtra(EXTRA_INSTITUTION);
        TextView fn=findViewById(R.id.fn);
        fn.setText(String.valueOf(fullName));
        TextView em=findViewById(R.id.emailRes);
        em.setText(String.valueOf(email));
        TextView ph=findViewById(R.id.telRes);
        ph.setText(String.valueOf(phone));
        TextView ins=findViewById(R.id.inst);
        ins.setText(String.valueOf(institution));
    }
}
