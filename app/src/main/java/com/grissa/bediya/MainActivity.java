package com.grissa.bediya;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText editFName,editLName,editInstitution,editEmail,editPhoneNumber,editpw,editpwC;
    private String fName,lName,institution,email,phoneNumber,pw,pwC;
    private RadioGroup gender;
    private boolean isMale=true,emptyField=false;
    private Button confirm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gender=findViewById(R.id.gender);
        gender.check(isMale? R.id.male : R.id.female);
        gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isMale=checkedId==R.id.male;
            }
        });
        editFName=findViewById(R.id.fName);
        editLName=findViewById(R.id.lName);
        editInstitution=findViewById(R.id.institution);
        editEmail=findViewById(R.id.email);
        editPhoneNumber=findViewById(R.id.phoneNbr);
        editpw=findViewById(R.id.pw);
        editpwC=findViewById(R.id.pwConf);
        confirm=findViewById(R.id.confirmBtn);
    }

    public boolean validate(){
        emptyField=false;
        pw=editpw.getText().toString();
        pwC=editpwC.getText().toString();
        if(pw.length()<6){
            CharSequence text="Make a Longer Password!";
            Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!pw.equals(pwC)){
            CharSequence text="Password does not match!";
            Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
            return false;
        }
        else{
            institution=editInstitution.getText().toString();
            if(institution.length()==0){
                CharSequence text="Institution is Empty";
                Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
                emptyField=true;
            }
            email=editEmail.getText().toString();
            if(email.length()==0){
                CharSequence text="Email is Empty";
                Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
                emptyField=true;
            }
            phoneNumber=editPhoneNumber.getText().toString();
            if(phoneNumber.length()==0){
                CharSequence text="Phone Number is Empty";
                Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
                emptyField=true;
            }
            lName=editLName.getText().toString();
            if(lName.length()==0){
                CharSequence text="Last Name is Empty";
                Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
                emptyField=true;
            }
            fName=editFName.getText().toString();
            if(fName.length()==0){
                CharSequence text="First Name is Empty";
                Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
                emptyField=true;
            }
            return !emptyField;
        }
    }

    public void onSubmit(View view){

        if(validate()){
            Intent intent=new Intent(this, ShowInfos.class);
            intent.putExtra(ShowInfos.EXTRA_FULL_NAME,fName+" "+lName);
            intent.putExtra(ShowInfos.EXTRA_PHONE_NUMBER,phoneNumber);
            intent.putExtra(ShowInfos.EXTRA_EMAIL,email);
            intent.putExtra(ShowInfos.EXTRA_INSTITUTION,institution);
            intent.putExtra(ShowInfos.EXTRA_IS_MALE,isMale);
            startActivity(intent);
        }
    }

}
